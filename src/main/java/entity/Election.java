/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author enriq
 */
@Entity
@Table(name = "election")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Election.findAll", query = "SELECT e FROM Election e")
    , @NamedQuery(name = "Election.findByCategory", query = "SELECT e FROM Election e WHERE e.electionCategoryId = :ec")
    , @NamedQuery(name = "Election.findById", query = "SELECT e FROM Election e WHERE e.id = :id")
    , @NamedQuery(name = "Election.findByName", query = "SELECT e FROM Election e WHERE e.name = :name")
    , @NamedQuery(name = "Election.findByDescription", query = "SELECT e FROM Election e WHERE e.description = :description")
    , @NamedQuery(name = "Election.findByStartDate", query = "SELECT e FROM Election e WHERE e.startDate = :startDate")
    , @NamedQuery(name = "Election.findByEndDate", query = "SELECT e FROM Election e WHERE e.endDate = :endDate")})
public class Election implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 25)
    @Column(name = "name")
    private String name;
    @Size(max = 50)
    @Column(name = "description")
    private String description;
    @Size(max = 45)
    @Column(name = "start_date")
    private String startDate;
    @Size(max = 45)
    @Column(name = "end_date")
    private String endDate;
    @JoinColumn(name = "election_category_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ElectionCategory electionCategoryId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "electionId")
    private List<Vote> voteList;

    public Election() {
    }

    public Election(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public ElectionCategory getElectionCategoryId() {
        return electionCategoryId;
    }

    public void setElectionCategoryId(ElectionCategory electionCategoryId) {
        this.electionCategoryId = electionCategoryId;
    }

    @XmlTransient
    public List<Vote> getVoteList() {
        return voteList;
    }

    public void setVoteList(List<Vote> voteList) {
        this.voteList = voteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Election)) {
            return false;
        }
        Election other = (Election) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Election[ id=" + id + " ]";
    }

}
