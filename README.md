# Telus Voting online website #

Developer test to evaluate technical skills in web applications.

##Database Design ##
>For more detailed information you can open the design file with MySQL Workbench windows application

![MODEL DIAGRAM.jpg](https://bitbucket.org/repo/8zXXXBk/images/3603815048-MODEL%20DIAGRAM.jpg)


### Requeriments ###

* NetBeans IDE 8.2
* JDK 1.8
* JEE 7
* MySQL Server
* GlassFish application server 4

## Setting up project ##

### Openning project###

* Clone this repository

     ` $ git clone https://enriquenery@bitbucket.org/enriquenery/telus-voting.git`

* Open maven project

     ![Open Project.jpg](https://bitbucket.org/repo/8zXXXBk/images/850658660-Open%20Project.jpg)
     
     ![project open.jpg](https://bitbucket.org/repo/8zXXXBk/images/1290662487-project%20open.jpg)

## Loading database ##

* Creating database 
> For the creation of the database I used the PHPMyAdmin web app.

      ![database creation.png](https://bitbucket.org/repo/8zXXXBk/images/183952351-database%20creation.png)

* Loading database dump 
> The dump contains the creation of database tables and some initial values for testing purposes

    ![import database.png](https://bitbucket.org/repo/8zXXXBk/images/3919764001-import%20database.png)

    ![import success.jpg](https://bitbucket.org/repo/8zXXXBk/images/2662363854-import%20success.jpg)

## Installing GlassFish Application Server ##
> Netbeans IDE 8.2 (My personal case) comes with a `GlassFish 4.1.1` distribution, but it seems like it has some kind of bug (It doesn´t let me create `jdbc-connection-pool` and `jdbc-resource` elements wich are necessary for the persistence engine to work.

## GlassFish installation steps ##

1. Go to Netbeans -> Services toolbar -> Servers -> Add Server

2.  Select `GlassFish server` and click Next

3.  Select ´Local Domain´ option and start server download

   ![Glassfish new GF serverç.jpg](https://bitbucket.org/repo/8zXXXBk/images/685256434-Glassfish%20new%20GF%20server%C3%A7.jpg)

### GlassFish configuration steps ###

>The configuration process is neccessary for the ´Telus Voting app´ to run properly.

 1. Create a new `jdbc-connection-pool`
>Go to Netbeans -> Services toolbar -> Servers -> `GlassFish (server recently installed)`, right click and select: `View admin console`
> `Resources` -> `JDBC` -> `JDBC Connection Pools` -> `New`, and set the values like the following image

 ![new.jpg](https://bitbucket.org/repo/8zXXXBk/images/3505893713-new.jpg)  

>NOTE: The database credentials and values has to be setted like your MySQL Database server

2. Create new `JDBC Resource`
> After the ´jdbc-connection-pool´ has been created is necessary the creation of a `JDBC Resource`.

 ![new jdbc resource.jpg](https://bitbucket.org/repo/8zXXXBk/images/618295464-new%20jdbc%20resource.jpg)

> NOTE: this element name has to match with the `<jta-data-source>` tag value in the `persistence.xml` inside the Java project. (If it doesn´t match the application will not run).

> For more information related to how to create Glassfish `Connection Pools` and `JDBC Resources` click [here](http://www.javaxt.com/Tutorials/Glassfish/How_to_set_up_a_Connection_Pool)

3. Restart the `GlassFish` application server. 

### Solving possible issues ###

>It can be possible that the `GlassFish` application server can have some bugs when saving the `Connection pools` and `Resources`. If that happens you can configure directly >inside the `domain.xml` configuration file of Glassfish. to look where that file is located, On Netbeans just make: `GlassFish installed server` -> `Properties` and look for >`Domains folder` location. Example:

> ![config.png](https://bitbucket.org/repo/8zXXXBk/images/2880921765-config.png)    

>And finally, add the needed configuration tags and values for: `jdbc-connection-pool` and `jdbc-resource` tags. Example:

![xml config.png](https://bitbucket.org/repo/8zXXXBk/images/3241962439-xml%20config.png)


## Project running ##
> Before running the project, it needs to download all of it´s dependencies and libraries. 

* Netbeans running project steps:

1.  Right click `telus-voting` project-> `Build with dependencies`:

  ![build with dependences.jpg](https://bitbucket.org/repo/8zXXXBk/images/1231179699-build%20with%20dependences.jpg)

2. Right click `telus-voting` project -> `Run`:
  
  ![Run.jpg](https://bitbucket.org/repo/8zXXXBk/images/2407302616-Run.jpg)

##TELUS VOTING APPLICATION##
###Login Module###
>The elections are based on a Election Category for example "Presidential elections" and a subcategory like "Salvadorian 2017 elections"

![login.jpg](https://bitbucket.org/repo/8zXXXBk/images/1368602416-login.jpg)

### Welcome Screen##
> It displays the registered user name and surname. The Election process selected title is displayed in the Header.

![wellcome.jpg](https://bitbucket.org/repo/8zXXXBk/images/3762106851-wellcome.jpg)


### Vote Module ###

> The register user can vote only once for the candidate.

 ![duplicate registry.jpg](https://bitbucket.org/repo/8zXXXBk/images/2772374604-duplicate%20registry.jpg)

### Stats Module (Report Module)###

>The stats are ajax based and changes dinamically

![report.jpg](https://bitbucket.org/repo/8zXXXBk/images/2689639580-report.jpg)

### Visitor´s counter ###
> The register file is called: `HITCOUNTER.txt` and is saved in `C:\` . The value retrived from the file is displayed in the Header of the App.

![Admin login.jpg](https://bitbucket.org/repo/8zXXXBk/images/4107089845-Admin%20login.jpg)

### Telus Voting Web Services ###
> The DELIVERABLE `/telus-voting/rest/VoteService/votes` web service delivers all of the votes made. 

![Vote WS.jpg](https://bitbucket.org/repo/8zXXXBk/images/2377833535-Vote%20WS.jpg)

> There are web services for all of the entities if they are needed. Example: 

![Country WS.jpg](https://bitbucket.org/repo/8zXXXBk/images/2662158079-Country%20WS.jpg)

##ONE MORE THING...##

###Administrator module, "hidden" feature####
>At login. Enter with the following credentials: 

> * Username: "admin"

> * Password: "admin"

>it will give you access to the tables management from Telus application. So you don´t need to go to the database to do tables administration.

![Admin menu.jpg](https://bitbucket.org/repo/8zXXXBk/images/1110480042-Admin%20menu.jpg)

![Admin submenu.jpg](https://bitbucket.org/repo/8zXXXBk/images/4191119127-Admin%20submenu.jpg)

![Person CRUD.jpg](https://bitbucket.org/repo/8zXXXBk/images/686937463-Person%20CRUD.jpg)