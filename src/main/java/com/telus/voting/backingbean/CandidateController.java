package com.telus.voting.backingbean;

import com.telus.voting.backingbean.util.MobilePageController;
import entity.Candidate;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "candidateController")
@ViewScoped
public class CandidateController extends AbstractController<Candidate> {

    @Inject
    private CommitteeController committeeIdController;
    @Inject
    private PersonController personIdController;
    @Inject
    private MobilePageController mobilePageController;

    public CandidateController() {
        // Inform the Abstract parent controller of the concrete Candidate Entity
        super(Candidate.class);
    }

    @PostConstruct
    public void init() {
        this.setSelected(new Candidate());
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        committeeIdController.setSelected(null);
        personIdController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Committee controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareCommitteeId(ActionEvent event) {
        if (this.getSelected() != null && committeeIdController.getSelected() == null) {
            committeeIdController.setSelected(this.getSelected().getCommitteeId());
        }
    }

    /**
     * Sets the "selected" attribute of the Person controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void preparePersonId(ActionEvent event) {
        if (this.getSelected() != null && personIdController.getSelected() == null) {
            personIdController.setSelected(this.getSelected().getPersonId());
        }
    }

    /**
     * Sets the "items" attribute with a collection of Vote entities that are
     * retrieved from Candidate?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Vote page
     */
    public String navigateVoteList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Vote_items", this.getSelected().getVoteList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/vote/index";
    }

}
