package com.telus.voting.backingbean;

import com.telus.voting.backingbean.util.MobilePageController;
import entity.Committee;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "committeeController")
@ViewScoped
public class CommitteeController extends AbstractController<Committee> {

    @Inject
    private DepartmentController departmentIdController;
    @Inject
    private MobilePageController mobilePageController;

    public CommitteeController() {
        // Inform the Abstract parent controller of the concrete Committee Entity
        super(Committee.class);
    }

    @PostConstruct
    public void init() {
        this.setSelected(new Committee());
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        departmentIdController.setSelected(null);
    }

    /**
     * Sets the "items" attribute with a collection of Candidate entities that
     * are retrieved from Committee?cap_first and returns the navigation
     * outcome.
     *
     * @return navigation outcome for Candidate page
     */
    public String navigateCandidateList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Candidate_items", this.getSelected().getCandidateList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/candidate/index";
    }

    /**
     * Sets the "selected" attribute of the Department controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareDepartmentId(ActionEvent event) {
        if (this.getSelected() != null && departmentIdController.getSelected() == null) {
            departmentIdController.setSelected(this.getSelected().getDepartmentId());
        }
    }
}
