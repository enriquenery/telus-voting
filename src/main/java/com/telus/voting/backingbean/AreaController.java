package com.telus.voting.backingbean;

import com.telus.voting.backingbean.util.MobilePageController;
import entity.Area;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "areaController")
@ViewScoped
public class AreaController extends AbstractController<Area> {

    @Inject
    private CountryController countryIdController;
    @Inject
    private MobilePageController mobilePageController;

    public AreaController() {
        // Inform the Abstract parent controller of the concrete Area Entity
        super(Area.class);
    }

    @PostConstruct
    public void init() {
        this.setSelected(new Area());
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        countryIdController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Country controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareCountryId(ActionEvent event) {
        if (this.getSelected() != null && countryIdController.getSelected() == null) {
            countryIdController.setSelected(this.getSelected().getCountryId());
        }
    }

    /**
     * Sets the "items" attribute with a collection of Department entities that
     * are retrieved from Area?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Department page
     */
    public String navigateDepartmentList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Department_items", this.getSelected().getDepartmentList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/department/index";
    }

}
