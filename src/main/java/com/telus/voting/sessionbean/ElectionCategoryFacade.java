/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telus.voting.sessionbean;

import entity.ElectionCategory;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author enriq
 */
@Stateless
public class ElectionCategoryFacade extends AbstractFacade<ElectionCategory> {

    @PersistenceContext(unitName = "com.telus.voting_telus-voting_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ElectionCategoryFacade() {
        super(ElectionCategory.class);
    }
    
}
