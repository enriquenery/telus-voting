/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telus.voting.backingbean;

import com.telus.voting.sessionbean.AreaFacade;
import com.telus.voting.sessionbean.DepartmentFacade;
import entity.Area;
import entity.Country;
import entity.Department;
import entity.Person;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.inject.Inject;

/**
 *
 * @author enriq
 */
@Named(value = "registerController")
@SessionScoped
public class RegisterController extends AbstractController<Person> {

    //VARIABLE DECLARATIONS
    private Person person = new Person();

    //Selected Item variable declaration
    private Country selectedCountry = new Country();
    private Area selectedArea = new Area();
    private Department selectedDepartment = new Department();

    //Selected Result Lists
    private List<Area> areaList = new <Area>ArrayList();
    private List<Department> departmentList = new <Department>ArrayList();

    //INJECTIONS
    @Inject
    private DepartmentFacade departmentFacade;
    @Inject
    private AreaFacade areaFacade;

    //Controllers
    @Inject
    private CountryController countryController;
    @Inject
    private AreaController areaController;
    @Inject
    private DepartmentController departmentIdController;
        @Inject
    private PersonController personController;

    /**
     * Creates a new instance of RegisterController
     */
    public RegisterController() {
        // Inform the Abstract parent controller of the concrete Person Entity
        super(Person.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        countryController.setSelected(null);
        areaController.setSelected(null);
        departmentIdController.setSelected(null);
    }

    public void register() {
        selectedArea.setCountryId(selectedCountry);
        selectedDepartment.setAreaId(selectedArea);
        person.setDepartmentId(selectedDepartment);
        
        this.setSelected(personController.getSelected()); 

    }

    //Ajax methods
    public void onCountryChange() {
     this.areaList = areaFacade.findByCountry(selectedCountry);
    }
    
    public void onAreaChange(){
    this.departmentList = departmentFacade.findByArea(selectedArea);
    }
    
    //GETTERS AND SETTERS

    public Country getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(Country selectedCountry) {
        this.selectedCountry = selectedCountry;
    }

    public Area getSelectedArea() {
        return selectedArea;
    }

    public void setSelectedArea(Area selectedArea) {
        this.selectedArea = selectedArea;
    }

    public Department getSelectedDepartment() {
        return selectedDepartment;
    }

    public void setSelectedDepartment(Department selectedDepartment) {
        this.selectedDepartment = selectedDepartment;
    }

    public List<Area> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<Area> areaList) {
        this.areaList = areaList;
    }

    public List<Department> getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }
    

}
