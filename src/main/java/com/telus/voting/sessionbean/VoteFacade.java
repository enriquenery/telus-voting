/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telus.voting.sessionbean;

import entity.Candidate;
import entity.Election;
import entity.Vote;
import entity.VoteStat;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author enriq
 */
@Stateless
public class VoteFacade extends AbstractFacade<Vote> {

    @PersistenceContext(unitName = "com.telus.voting_telus-voting_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VoteFacade() {
        super(Vote.class);
    }

    public List<Vote> findByElectionCategory(Election election) {
        List<Vote> listaElection = new <Vote>ArrayList();
        //query base
        Query nq = getEntityManager().createNamedQuery("Vote.findByElection");
        nq.setParameter("e", election);
        //paginacion
        listaElection = nq.getResultList();
        return listaElection;
    }

    public List<VoteStat> findStatsByElectionCategory(Election election) {
        List<Object[]> listaObjetos = new <Object>ArrayList();
        List<VoteStat> listaElection = new <VoteStat>ArrayList();
        //query base
        Query nq = getEntityManager().createNamedQuery("Vote.findStatByElection");
        nq.setParameter("e", election);
        //paginacion
        listaObjetos = nq.getResultList();
        for (Object o[] : listaObjetos) {
            VoteStat vs = new VoteStat();
            vs.setCandidate((Candidate) o[0]);
            vs.setTotalVotes((Long) o[1]);
            System.err.println("VOTE STAT: Candidate: " + vs.getCandidate() + " TotalVotes: " + vs.getTotalVotes());
            listaElection.add(vs);
        }
        return listaElection;
    }

}
