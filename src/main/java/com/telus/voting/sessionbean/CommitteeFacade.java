/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telus.voting.sessionbean;

import entity.Committee;
import entity.Person;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author enriq
 */
@Stateless
public class CommitteeFacade extends AbstractFacade<Committee> {

    @PersistenceContext(unitName = "com.telus.voting_telus-voting_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CommitteeFacade() {
        super(Committee.class);
    }

    public List<Committee> findByCommittee(Person person) {
        List<Committee> committeeList = new <Committee>ArrayList();
        //query base
        Query nq = getEntityManager().createNamedQuery("Committee.findByDepartmentId");
        nq.setParameter("id", person.getDepartmentId());
        //paginacion
        committeeList = nq.getResultList();
        return committeeList;
    }

}
