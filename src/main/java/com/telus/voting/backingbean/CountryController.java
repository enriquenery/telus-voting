package com.telus.voting.backingbean;

import com.telus.voting.backingbean.util.MobilePageController;
import entity.Country;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "countryController")
@ViewScoped
public class CountryController extends AbstractController<Country> {

    @Inject
    private MobilePageController mobilePageController;

    public CountryController() {
        // Inform the Abstract parent controller of the concrete Country Entity
        super(Country.class);
    }

    @PostConstruct
    public void init() {
        this.setSelected(new Country());
    }

    /**
     * Sets the "items" attribute with a collection of Area entities that are
     * retrieved from Country?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Area page
     */
    public String navigateAreaList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Area_items", this.getSelected().getAreaList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/area/index";
    }

}
