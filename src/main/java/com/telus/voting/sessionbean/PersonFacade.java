/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telus.voting.sessionbean;

import entity.Person;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author enriq
 */
@Stateless
public class PersonFacade extends AbstractFacade<Person> {

    @PersistenceContext(unitName = "com.telus.voting_telus-voting_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PersonFacade() {
        super(Person.class);
    }

    public boolean findByIDPerson(Person person) {
        boolean result = false;
        Person personResult = new Person();
        //query base
        Query nq = getEntityManager().createNamedQuery("Person.findByUid");
        nq.setParameter("uid", person.getUid());
        //paginacion
        personResult = (Person) nq.getSingleResult();
        System.err.println("FUERA DE LA CONDICION pUserId: " + person.getUid() + " pResult.Id: " + personResult.getUid() + "pPass: " + person.getPassword() + " pRPass: " + personResult.getPassword());
        if (person.getUid().equals(personResult.getUid()) && person.getPassword().equals(personResult.getPassword())) {
            result = true;
        }
        return result;
    }

    public Person findByUID(String username) {
        Person personResult = new Person();
        //query base
        Query nq = getEntityManager().createNamedQuery("Person.findByUid");
        nq.setParameter("uid", username);
        //paginacion
        personResult = (Person) nq.getSingleResult();
        return personResult;
    }

}
