package com.telus.voting.backingbean;

import com.telus.voting.backingbean.util.MobilePageController;
import com.telus.voting.backingbean.util.Sesion;
import com.telus.voting.sessionbean.CandidateFacade;
import com.telus.voting.sessionbean.CommitteeFacade;
import entity.Candidate;
import entity.Committee;
import entity.Vote;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "voteController")
@SessionScoped
public class VoteController extends AbstractController<Vote> {

    //Selected elements
    private Committee selectedCommittee = new Committee();
    private Candidate selectedCandidate = new Candidate();

    //Result lists of the selected elements
    private List<Committee> commiteeList = new <Committee>ArrayList();
    private List<Candidate> candidateList = new <Candidate>ArrayList();

    //First two injections for voting process
    @Inject
    private CommitteeFacade committeeFacade;
    @Inject
    private CandidateFacade candidateFacade;

    @Inject
    private CommitteeController committeeController;
    @Inject
    private ElectionController electionIdController;
    @Inject
    private CandidateController candidateIdController;
    @Inject
    private PersonController personIdController;
    @Inject
    private MobilePageController mobilePageController;

    public VoteController() {
        // Inform the Abstract parent controller of the concrete Vote Entity
        super(Vote.class);
    }

    @PostConstruct
    public void init() {
        this.setSelected(new Vote());
        findCommitteeOfPersonDepartment();
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        electionIdController.setSelected(null);
        candidateIdController.setSelected(null);
        personIdController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Election controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareElectionId(ActionEvent event) {
        if (this.getSelected() != null && electionIdController.getSelected() == null) {
            electionIdController.setSelected(this.getSelected().getElectionId());
        }
    }

    /**
     * Sets the "selected" attribute of the Candidate controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareCandidateId(ActionEvent event) {
        if (this.getSelected() != null && candidateIdController.getSelected() == null) {
            candidateIdController.setSelected(this.getSelected().getCandidateId());
        }
    }

    /**
     * Sets the "selected" attribute of the Person controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void preparePersonId(ActionEvent event) {
        if (this.getSelected() != null && personIdController.getSelected() == null) {
            personIdController.setSelected(this.getSelected().getPersonId());
        }
    }

    public void findCommitteeOfPersonDepartment() {
        Sesion sesion = new Sesion();
        commiteeList = committeeFacade.findByCommittee(sesion.personSession());
    }

    public void onCommitteeChange() {
        candidateList = candidateFacade.findByCommittee(selectedCommittee);
    }

    public void vote() {

        //Invoke Session values
        Sesion sesion = new Sesion();

        //Values to register in voting process
        Date date = new Date(System.currentTimeMillis());

        this.getSelected().setDate(date);
        this.getSelected().setElectionId(sesion.electionSession());
        this.getSelected().setPersonId(sesion.personSession());
        this.getSelected().setCandidateId(selectedCandidate);

        this.saveNew();
    }

    //GETTERS AND SETTERS
    public Committee getSelectedCommittee() {
        return selectedCommittee;
    }

    public void setSelectedCommittee(Committee selectedCommittee) {
        this.selectedCommittee = selectedCommittee;
    }

    public Candidate getSelectedCandidate() {
        return selectedCandidate;
    }

    public void setSelectedCandidate(Candidate selectedCandidate) {
        this.selectedCandidate = selectedCandidate;
    }

    public List<Committee> getCommiteeList() {
        return commiteeList;
    }

    public void setCommiteeList(List<Committee> commiteeList) {
        this.commiteeList = commiteeList;
    }

    public List<Candidate> getCandidateList() {
        return candidateList;
    }

    public void setCandidateList(List<Candidate> candidateList) {
        this.candidateList = candidateList;
    }

}
