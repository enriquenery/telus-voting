/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telus.voting.sessionbean;

import entity.Area;
import entity.Department;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author enriq
 */
@Stateless
public class DepartmentFacade extends AbstractFacade<Department> {

    @PersistenceContext(unitName = "com.telus.voting_telus-voting_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DepartmentFacade() {
        super(Department.class);
    }
    
    public List<Department> findByArea(Area area) {
        List<Department> departmentList = new <Department>ArrayList();
        //query base
        Query nq = getEntityManager().createNamedQuery("Department.findByArea");
        nq.setParameter("id", area);
        //paginacion
        departmentList = nq.getResultList();
        return departmentList;
    }
    
}
