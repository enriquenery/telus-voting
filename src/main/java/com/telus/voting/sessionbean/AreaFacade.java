/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telus.voting.sessionbean;

import entity.Area;
import entity.Country;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author enriq
 */
@Stateless
public class AreaFacade extends AbstractFacade<Area> {

    @PersistenceContext(unitName = "com.telus.voting_telus-voting_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AreaFacade() {
        super(Area.class);
    }

    public List<Area> findByCountry(Country country) {
        List<Area> listaArea = new <Area>ArrayList();
        //query base
        Query nq = getEntityManager().createNamedQuery("Area.findByCountry");
        nq.setParameter("id", country);
        //paginacion
        listaArea = nq.getResultList();
        return listaArea;
    }

}
