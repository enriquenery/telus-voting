/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telus.voting.ws; 

import com.telus.voting.sessionbean.VoteFacade;
import entity.Vote;
import java.io.Serializable;
import java.util.List; 
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET; 
import javax.ws.rs.Path; 
import javax.ws.rs.Produces; 
import javax.ws.rs.core.MediaType;  

@Path("/VoteService") 
@Named(value = "votesWS")
@Stateless
public class VoteService implements Serializable {  
   
    @Inject
    private VoteFacade vf;

    public VoteService() {
    }

  
    
    
    
   @GET 
   @Path("/votes") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<Vote> getVotes(){ 
      return vf.findAll();
   }  
}
