-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: telus-voting
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.16-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Area identifier.',
  `name` varchar(25) DEFAULT NULL COMMENT 'Name of the area.',
  `description` varchar(50) DEFAULT NULL COMMENT 'A short description of the area.',
  `country_id` int(11) NOT NULL COMMENT 'Country of the Area.',
  PRIMARY KEY (`id`),
  KEY `fk_area_country1_idx` (`country_id`),
  CONSTRAINT `fk_area_country1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area`
--

LOCK TABLES `area` WRITE;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` VALUES (1,'Occidental','Occidental salvadorian departments',100),(2,'Central','Central salvadorian departments',100),(3,'Oriental','Oriental salvadorian departments',100);
/*!40000 ALTER TABLE `area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate`
--

DROP TABLE IF EXISTS `candidate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique candidate identifier.',
  `email` varchar(45) DEFAULT NULL COMMENT 'Contact email of the candidate.',
  `committee_id` int(11) NOT NULL COMMENT 'Committee of the candidate.',
  `person_id` int(11) NOT NULL COMMENT 'Personal information of the candidate.',
  PRIMARY KEY (`id`),
  KEY `fk_candidate_committee1_idx` (`committee_id`),
  KEY `fk_candidate_person1_idx` (`person_id`),
  CONSTRAINT `fk_candidate_committee1` FOREIGN KEY (`committee_id`) REFERENCES `committee` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_candidate_person1` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate`
--

LOCK TABLES `candidate` WRITE;
/*!40000 ALTER TABLE `candidate` DISABLE KEYS */;
INSERT INTO `candidate` VALUES (1,'enriquenery',2,1),(2,'name@mail.com',2,2),(3,'name@mail.com',2,3),(4,'name@mail.com',1,4),(5,'name@mail.com',1,5);
/*!40000 ALTER TABLE `candidate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `committee`
--

DROP TABLE IF EXISTS `committee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `committee` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier of the committee',
  `name` varchar(45) DEFAULT NULL COMMENT 'Name of the committee',
  `department_id` int(11) NOT NULL COMMENT 'Department of the committee (If a committee belongs to an area of a country, just select the first department that is related to the area you want)',
  PRIMARY KEY (`id`),
  KEY `fk_committee_department1_idx` (`department_id`),
  CONSTRAINT `fk_committee_department1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `committee`
--

LOCK TABLES `committee` WRITE;
/*!40000 ALTER TABLE `committee` DISABLE KEYS */;
INSERT INTO `committee` VALUES (1,'San Salvador Committee',5),(2,'Cabañas Committee',9);
/*!40000 ALTER TABLE `committee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique country identifier.',
  `code` varchar(10) DEFAULT NULL COMMENT 'Code of the country.',
  `name` varchar(45) DEFAULT NULL COMMENT 'Name of the country.',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'IND','India'),(2,'GHA','Ghana'),(3,'GEO','Georgia'),(4,'NZL','New Zealand'),(5,'LBN','Lebanon'),(6,'UKR','Ukraine'),(7,'COL','Colombia'),(8,'URY','Uruguay'),(9,'NPL','Nepal'),(10,'BGR','Bulgaria'),(11,'YEM','Yemen'),(12,'IRL','Ireland'),(13,'BGD','Bangladesh'),(14,'BEL','Belgium'),(15,'LUX','Luxembourg'),(16,'HRV','Croatia'),(17,'VNM','Viet Nam'),(18,'BWA','Botswana'),(19,'ZAF','South Africa'),(20,'FRA','France'),(21,'LKA','Sri Lanka'),(22,'SGP','Singapore'),(23,'CZE','Czech Republic'),(24,'ROU','Romania'),(25,'FIN','Finland'),(26,'BRA','Brazil'),(27,'AUS','Australia'),(28,'SLE','Sierra Leone'),(29,'GUY','Guyana'),(30,'ITA','Italy'),(31,'CYP','Cyprus'),(32,'SVK','Slovakia'),(33,'DNK','Denmark'),(34,'IDN','Indonesia'),(35,'SUR','Suriname'),(36,'MEX','Mexico'),(37,'NIC','Nicaragua'),(38,'THA','Thailand'),(39,'ZWE','Zimbabwe'),(40,'GMB','Gambia'),(41,'MLT','Malta'),(42,'LVA','Latvia'),(43,'KWT','Kuwait'),(44,'PRT','Portugal'),(45,'PER','Peru'),(46,'IRQ','Iraq'),(47,'ARG','Argentina'),(48,'MWI','Malawi'),(49,'BLZ','Belize'),(50,'ECU','Ecuador'),(51,'PRY','Paraguay'),(52,'MYS','Malaysia'),(53,'ZMB','Zambia'),(54,'TUN','Tunisia'),(55,'ALB','Albania'),(56,'TUR','Turkey'),(57,'ISR','Israel'),(58,'PHL','Philippines'),(59,'AUT','Austria'),(60,'MRT','Mauritania'),(61,'SVN','Slovenia'),(62,'UGA','Uganda'),(63,'CAN','Canada'),(64,'MUS','Mauritius'),(65,'WSM','Samoa'),(66,'CHL','Chile'),(67,'SWZ','Swaziland'),(68,'ESP','Spain'),(69,'CUB','Cuba'),(70,'MNG','Mongolia'),(71,'KEN','Kenya'),(72,'GTM','Guatemala'),(73,'DEU','Germany'),(74,'QAT','Qatar'),(75,'NOR','Norway'),(76,'CHE','Switzerland'),(77,'NLD','Netherlands'),(78,'PAK','Pakistan'),(79,'HUN','Hungary'),(80,'PAN','Panama'),(81,'NAM','Namibia'),(82,'JPN','Japan'),(83,'JOR','Jordan'),(84,'BHR','Bahrain'),(85,'USA','United States'),(86,'MMR','Myanmar'),(87,'MDA','Moldova'),(88,'ISL','Iceland'),(89,'MAR','Morocco'),(90,'SAU','Saudi Arabia'),(91,'POL','Poland'),(92,'SWE','Sweden'),(93,'KOR','Korea'),(94,'AFG','Afghanistan'),(95,'LTU','Lithuania'),(96,'FJI','Fiji'),(97,'NGA','Nigeria'),(98,'EST','Estonia'),(99,'GBR','United Kingdom'),(100,'SLV','El Salvador');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique departement identifier.',
  `name` varchar(25) DEFAULT NULL COMMENT 'Name of the department.',
  `area_id` int(11) NOT NULL COMMENT 'Area of the department.',
  PRIMARY KEY (`id`),
  KEY `fk_department_area1_idx` (`area_id`),
  CONSTRAINT `fk_department_area1` FOREIGN KEY (`area_id`) REFERENCES `area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (1,'Ahuachapan',1),(2,'Sonsonate',1),(3,'Santa Ana',1),(4,'La Libertad',2),(5,'San Salvador',2),(6,'Chalatenango',2),(7,'Cuscatlan',2),(8,'La Paz',2),(9,'Cabañas',2),(10,'San Vicente ',2),(11,'Usulután',3),(12,'San Miguel',3),(13,'Morazán',3),(14,'La Unión',3);
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `election`
--

DROP TABLE IF EXISTS `election`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `election` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier for the election.',
  `name` varchar(25) DEFAULT NULL COMMENT 'Title of the election.',
  `description` varchar(50) DEFAULT NULL COMMENT 'A short description of the election.',
  `start_date` varchar(45) DEFAULT NULL COMMENT 'Date when the election begins.',
  `end_date` varchar(45) DEFAULT NULL COMMENT 'Date when the election ends.',
  `election_category_id` int(11) NOT NULL COMMENT 'Category of the election.',
  PRIMARY KEY (`id`),
  KEY `fk_election_election_category1_idx` (`election_category_id`),
  CONSTRAINT `fk_election_election_category1` FOREIGN KEY (`election_category_id`) REFERENCES `election_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `election`
--

LOCK TABLES `election` WRITE;
/*!40000 ALTER TABLE `election` DISABLE KEYS */;
INSERT INTO `election` VALUES (1,'Salvadorian 2017 election','Election process for salvadorian population','2017/01/01','2017/01/05',1),(2,'Salvadorian 2017 deputies','Election for 2017 salvadorian diputies','2017/01/01','201/01/05',2);
/*!40000 ALTER TABLE `election` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `election_category`
--

DROP TABLE IF EXISTS `election_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `election_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier for the election category.',
  `name` varchar(20) DEFAULT NULL COMMENT 'Title of the election',
  `description` varchar(50) DEFAULT NULL COMMENT 'A  short description of the election process ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `election_category`
--

LOCK TABLES `election_category` WRITE;
/*!40000 ALTER TABLE `election_category` DISABLE KEYS */;
INSERT INTO `election_category` VALUES (1,'Presidential','Presidential Elections '),(2,'Deputy','Deputy elections'),(3,'Mayor','Mayors elections');
/*!40000 ALTER TABLE `election_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier for the person (internal use)',
  `uid` varchar(30) DEFAULT NULL COMMENT 'Unique Identification Document number(for example DUI in El Salvador''s case), this will be their username.\n',
  `password` varchar(20) DEFAULT NULL COMMENT 'Password of the person.',
  `name` varchar(45) DEFAULT NULL COMMENT 'Names of the person.',
  `surname` varchar(45) DEFAULT NULL COMMENT 'Surnames of the person.',
  `department_id` int(11) NOT NULL COMMENT 'Department where the person lives.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_UNIQUE` (`uid`),
  KEY `fk_person_department1_idx` (`department_id`),
  CONSTRAINT `fk_person_department1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,'043316042','password','Milton Enrique ','Ramírez Nery',9),(2,'123456789','password123','Jhon','Perez',9),(3,'987654321','pass','Julian','Smith',9),(4,'987456321','enter','Carlos','Torrez',5),(5,'1122334455','pass','Michael','Heldens',5);
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vote`
--

DROP TABLE IF EXISTS `vote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique vote identifier.',
  `date` date DEFAULT NULL COMMENT 'Date when the vote is made.',
  `candidate_id` int(11) NOT NULL COMMENT 'Candidate selected.',
  `person_id` int(11) NOT NULL COMMENT 'Person who is voting.',
  `election_id` int(11) NOT NULL COMMENT 'Id of the election process.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_vote_index` (`candidate_id`,`person_id`,`election_id`),
  KEY `fk_votes_candidate1_idx` (`candidate_id`),
  KEY `fk_votes_person1_idx` (`person_id`),
  KEY `fk_vote_election1_idx` (`election_id`),
  CONSTRAINT `fk_vote_election1` FOREIGN KEY (`election_id`) REFERENCES `election` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_votes_candidate1` FOREIGN KEY (`candidate_id`) REFERENCES `candidate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_votes_person1` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vote`
--

LOCK TABLES `vote` WRITE;
/*!40000 ALTER TABLE `vote` DISABLE KEYS */;
INSERT INTO `vote` VALUES (1,'2017-05-27',1,1,1),(3,'2017-05-27',2,1,1);
/*!40000 ALTER TABLE `vote` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-27  8:10:20
