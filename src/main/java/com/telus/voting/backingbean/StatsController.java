/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telus.voting.backingbean;

import com.telus.voting.sessionbean.ElectionCategoryFacade;
import com.telus.voting.sessionbean.ElectionFacade;
import com.telus.voting.sessionbean.VoteFacade;
import entity.Election;
import entity.ElectionCategory;
import entity.Vote;
import entity.VoteStat;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author enriq
 */
@Named(value = "statsController")
@ViewScoped
public class StatsController extends AbstractController<Vote> implements Serializable {

    //INJECTS
    @Inject
    private ElectionCategoryFacade ecf;
    @Inject
    private ElectionFacade ef;
    @Inject
    private VoteFacade vf;

    //VARIABLES 
    //SelectedItems
    private ElectionCategory selectedElectionCategory = new ElectionCategory();
    private Election selectedElection = new Election();
    private List<Election> electionList = new <Election>ArrayList();
    private List<Vote> voteList = new <Vote> ArrayList();
    private List<VoteStat> voteStatList = new <VoteStat> ArrayList();

    private PieChartModel livePieModel = new PieChartModel();

    /**
     * Creates a new instance of StatsController
     */
    public StatsController() {
        // Inform the Abstract parent controller of the concrete Country Entity
        super(Vote.class);
          livePieModel = new PieChartModel();
    }

    @PostConstruct
    public void init() {
        livePieModel = new PieChartModel();
        livePieModel.getData().put("", 0);
        livePieModel.setTitle("Vote Stats.");
        livePieModel.setLegendPosition("ne");
        livePieModel.setDiameter(150);
    }

    public PieChartModel getLivePieModel() {
        livePieModel = new PieChartModel();
        livePieModel.getData().put("", 0);
        onElectionChange();
        //Llenado del diagrama
        for (int i = 0; i < voteStatList.size(); i++) {
            VoteStat get = voteStatList.get(i);
            livePieModel.getData().put(get.getCandidate().getPersonId().getName(), get.getTotalVotes());
        }

        livePieModel.setTitle(selectedElection.getName() ==null? "": selectedElection.getName()+ " Vote Stats.");
        livePieModel.setLegendPosition("ne");
        livePieModel.setShowDataLabels(true);
        livePieModel.setDiameter(150);
        return livePieModel;
    }

    public void setLivePieModel(PieChartModel livePieModel) {
        this.livePieModel = livePieModel;
    }

    //Methods
    public void onElectionCategoryChange() {
        this.electionList = ef.findByElectionCategory(selectedElectionCategory);

    }

    public void onElectionChange() {
        this.voteStatList = vf.findStatsByElectionCategory(selectedElection);
    }

    //GETTERS AND SETTERS
    public ElectionCategory getSelectedElectionCategory() {
        return selectedElectionCategory;
    }

    public void setSelectedElectionCategory(ElectionCategory selectedElectionCategory) {
        this.selectedElectionCategory = selectedElectionCategory;
    }

    public Election getSelectedElection() {
        return selectedElection;
    }

    public void setSelectedElection(Election selectedElection) {
        this.selectedElection = selectedElection;
    }

    public List<Election> getElectionList() {
        return electionList;
    }

    public void setElectionList(List<Election> electionList) {
        this.electionList = electionList;
    }

}
