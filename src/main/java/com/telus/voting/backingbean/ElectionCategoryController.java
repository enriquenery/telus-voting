package com.telus.voting.backingbean;

import com.telus.voting.backingbean.util.MobilePageController;
import entity.ElectionCategory;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "electionCategoryController")
@ViewScoped
public class ElectionCategoryController extends AbstractController<ElectionCategory> {

    @Inject
    private MobilePageController mobilePageController;

    public ElectionCategoryController() {
        // Inform the Abstract parent controller of the concrete ElectionCategory Entity
        super(ElectionCategory.class);
    }

    @PostConstruct
    public void init() {
        this.setSelected(new ElectionCategory());
    }

    /**
     * Sets the "items" attribute with a collection of Election entities that
     * are retrieved from ElectionCategory?cap_first and returns the navigation
     * outcome.
     *
     * @return navigation outcome for Election page
     */
    public String navigateElectionList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Election_items", this.getSelected().getElectionList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/election/index";
    }

}
