/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telus.voting.backingbean;

import com.telus.voting.backingbean.util.Sesion;
import com.telus.voting.filter.SiteHitCounter;
import com.telus.voting.sessionbean.PersonFacade;
import entity.Election;
import entity.Person;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;

/**
 *
 * @author enriq
 */
@Named(value = "loginView")
@SessionScoped
public class LoginView implements Serializable {

    //VARIABLES
    private String username = "";
    private String password = "";
    
    //Election variables
    private Election selectedElection = new Election();

    //INJECTIONS
    //Facades
    @Inject
    private PersonFacade pf;
    //Controllers
    @Inject
    private PersonController pc;

    /**
     * Creates a new instance of LoginView
     */
    public LoginView() {

    }

    @PostConstruct
    public void init() {
        username = new String();
        password = new String();
        createVisitorCounterSession();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void login(ActionEvent event) {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean loggedIn = false;

        System.err.println("USER: " + username + " PASSWORD: " + password);

        //FOR ADMINISTRATION VIEW
        if (username != null && username.equals("admin") && password != null && password.equals("admin")) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect(
                        FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/faces/admin_index.xhtml");
                loggedIn = true;
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Welcome", username);
            } catch (IOException ex) {
                Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            //FOR VOTERS VIEW
            Person person = new Person();
            person.setUid(username);
            person.setPassword(password);
            if (pf.findByIDPerson(person)) {
                //Voters login area here
                try {
                     context = RequestContext.getCurrentInstance();               
                    ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                    HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
                    HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
                    
                    //Session creation
                    HttpSession sesionOk = request.getSession();
                    sesionOk.setMaxInactiveInterval(600);//5 minutos maximo de inactividad
                    sesionOk.setAttribute(Sesion.sesionAdmin, pf.findByUID(username)); //Sets the person object for the session
                    sesionOk.setAttribute(Sesion.electionIdentifier, selectedElection); //Sets the person object for the session
                    
                    //Redirect to the next page
                    FacesContext.getCurrentInstance().getExternalContext().redirect(
                            FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/faces/voter_index.xhtml");
                    loggedIn = true;
                    message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Welcome", username);
                } catch (IOException ex) {
                    Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {

                //ERROR WITH CREDENTIALS ACCESS
                loggedIn = false;
                message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Loggin Error", "Invalid credentials");
            }
        }

        FacesContext.getCurrentInstance().addMessage(null, message);
        context.addCallbackParam("loggedIn", loggedIn);
    }
    
    public void createVisitorCounterSession(){
        
        SiteHitCounter shc = new SiteHitCounter();
    
                //Session creation
                RequestContext context = RequestContext.getCurrentInstance();
                ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
                HttpSession sesionOk = request.getSession();
                sesionOk.setMaxInactiveInterval(600);//5 minutos maximo de inactividad
                sesionOk.setAttribute(Sesion.visitorsCounter, shc.readFile()); //Sets the person object for the session
    }
    
    //GETTERS AND SETTERS

    public Election getSelectedElection() {
        return selectedElection;
    }

    public void setSelectedElection(Election selectedElection) {
        this.selectedElection = selectedElection;
    }
    

}
