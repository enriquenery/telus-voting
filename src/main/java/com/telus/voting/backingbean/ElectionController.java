package com.telus.voting.backingbean;

import com.telus.voting.backingbean.util.MobilePageController;
import entity.Election;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "electionController")
@ViewScoped
public class ElectionController extends AbstractController<Election> {

    @Inject
    private ElectionCategoryController electionCategoryIdController;
    @Inject
    private MobilePageController mobilePageController;

    public ElectionController() {
        // Inform the Abstract parent controller of the concrete Election Entity
        super(Election.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        electionCategoryIdController.setSelected(null);
    }

    @PostConstruct
    public void init() {
        this.setSelected(new Election());
    }

    /**
     * Sets the "selected" attribute of the ElectionCategory controller in order
     * to display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareElectionCategoryId(ActionEvent event) {
        if (this.getSelected() != null && electionCategoryIdController.getSelected() == null) {
            electionCategoryIdController.setSelected(this.getSelected().getElectionCategoryId());
        }
    }

    /**
     * Sets the "items" attribute with a collection of Vote entities that are
     * retrieved from Election?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Vote page
     */
    public String navigateVoteList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Vote_items", this.getSelected().getVoteList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/vote/index";
    }

}
