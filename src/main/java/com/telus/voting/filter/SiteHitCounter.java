/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telus.voting.filter;

import com.telus.voting.backingbean.util.Sesion;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;

/**
 *
 * @author enriq
 */
public class SiteHitCounter implements Filter {

    String fileName = "C://HITCOUNTER.txt";

    private int hitCount;

    public SiteHitCounter() {
    }

    public void init(FilterConfig config)
            throws ServletException {
        // Reset hit counter.
        hitCount = 0;
        readFile();
    }

    public void doFilter(ServletRequest request,
            ServletResponse response,
            FilterChain chain)
            throws java.io.IOException, ServletException {

        // increase counter by one
        hitCount++;

        // Print the counter.
        System.out.println("Site visits count :" + hitCount);

        // Pass request back down the filter chain
        chain.doFilter(request, response);
    }

    public void destroy() {
        try {
            // This is optional step but if you like you
            // can write hitCount value in your database.
            // read and update into file
            updateHitCounterFile();
        } catch (IOException ex) {
            Logger.getLogger(SiteHitCounter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void updateHitCounterFile() throws IOException {

        /**
         * Here I am increasing counter each time this HitCounterServlet is
         * called. I am updating HITCOUNTER.txt file which store total number of
         * visitors on website. 
         */
        // read and update into file
        File file = new File(fileName);

        // if file doesnt exists, then create it
        if (!file.exists()) {
            file.createNewFile();
        }
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("DATE: " + new Date(System.currentTimeMillis()) + " NUMBER OF HITS: " + Long.toString(hitCount));
        bw.close();
    }

    public String readFile() {
        BufferedReader br = null;
        String temp = "";
        try {
            br = new BufferedReader(new FileReader(fileName));
            while ((temp = br.readLine()) != null) {
                System.out.println("HIT COUNTER FILE:  " + temp);            
                return temp;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return temp;
    }
}
