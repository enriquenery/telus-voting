/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telus.voting.backingbean.util;

import entity.Election;
import entity.Person;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;


/**
 *
 * @author enriq
 */
@ManagedBean
@ViewScoped
public class Sesion {

    public static String sesionAdmin = "UserSesion"; //Name (indentifier or key) of the object saved in session (the value saved)
    public static String electionIdentifier = "ElectionSession"; //Name (indentifier or key) of the object saved in session (the value saved)
     public static String visitorsCounter = "VisitorsCounter"; //Name (indentifier or key) of the object saved in session (the value saved)
    
/**
 * Valida si el person se encuentra logueado
 */
    public boolean validarSesion() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        HttpSession sesionOk = request.getSession();
        String person = (String) sesionOk.getAttribute(Sesion.sesionAdmin);
        RequestContext context = RequestContext.getCurrentInstance();
        if (person == null) {
            //If the object saved in the session doesn't exists, then, redirectionate
            context.execute("window.location = \"" + request.getContextPath() + "/faces/login/login.xhtml\";");
            return false;
        }
        return true;
    }

    /**
     * Gets the element saved on session
     * */
    public static Person personSesion(){
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        HttpSession sesionOk = request.getSession();
        Person person = (Person) sesionOk.getAttribute(Sesion.sesionAdmin); 
        return person;
    }
    
    public  Person personSession(){
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        HttpSession sesionOk = request.getSession();
        Person person = (Person) sesionOk.getAttribute(Sesion.sesionAdmin); 
        return person;
    }
    
    public  Election electionSession(){
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        HttpSession sesionOk = request.getSession();
        Election election = (Election) sesionOk.getAttribute(Sesion.electionIdentifier); 
        return election;
    }
    
    
    public  String visitorsSession(){
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        HttpSession sesionOk = request.getSession();
        String election = (String) sesionOk.getAttribute(Sesion.visitorsCounter); 
        return election;
    }
}
