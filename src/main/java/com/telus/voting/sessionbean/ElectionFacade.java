/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telus.voting.sessionbean;

import entity.Election;
import entity.ElectionCategory;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author enriq
 */
@Stateless
public class ElectionFacade extends AbstractFacade<Election> {

    @PersistenceContext(unitName = "com.telus.voting_telus-voting_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ElectionFacade() {
        super(Election.class);
    }
    
    public List<Election> findByElectionCategory(ElectionCategory electionCategory) {
        List<Election> listaElection = new <Election>ArrayList();
        //query base
        Query nq = getEntityManager().createNamedQuery("Election.findByCategory");
        nq.setParameter("ec", electionCategory);
        //paginacion
        listaElection = nq.getResultList();
        return listaElection;
    }
    
}
