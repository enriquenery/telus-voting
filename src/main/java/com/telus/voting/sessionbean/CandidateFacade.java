/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telus.voting.sessionbean;

import entity.Candidate;
import entity.Committee;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author enriq
 */
@Stateless
public class CandidateFacade extends AbstractFacade<Candidate> {

    @PersistenceContext(unitName = "com.telus.voting_telus-voting_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CandidateFacade() {
        super(Candidate.class);
    }
    
    public List<Candidate> findByCommittee(Committee committee) {
        List<Candidate> candidateList = new <Candidate>ArrayList();
        //query base
        Query nq = getEntityManager().createNamedQuery("Candidate.findByCommittee");
        nq.setParameter("id", committee);
        //paginacion
        candidateList = nq.getResultList();
        return candidateList;
    }
    
}
