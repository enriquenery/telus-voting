-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema telus-voting
-- -----------------------------------------------------
-- Telus web application test with the aim to evaluate developer skills. 
DROP SCHEMA IF EXISTS `telus-voting` ;

-- -----------------------------------------------------
-- Schema telus-voting
--
-- Telus web application test with the aim to evaluate developer skills. 
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `telus-voting` DEFAULT CHARACTER SET utf8 ;
USE `telus-voting` ;

-- -----------------------------------------------------
-- Table `telus-voting`.`country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `telus-voting`.`country` ;

CREATE TABLE IF NOT EXISTS `telus-voting`.`country` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Unique country identifier.',
  `code` VARCHAR(10) NULL COMMENT 'Code of the country.',
  `name` VARCHAR(45) NULL COMMENT 'Name of the country.',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `telus-voting`.`area`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `telus-voting`.`area` ;

CREATE TABLE IF NOT EXISTS `telus-voting`.`area` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Unique Area identifier.',
  `name` VARCHAR(25) NULL COMMENT 'Name of the area.',
  `description` VARCHAR(50) NULL COMMENT 'A short description of the area.',
  `country_id` INT NOT NULL COMMENT 'Country of the Area.',
  PRIMARY KEY (`id`),
  INDEX `fk_area_country1_idx` (`country_id` ASC),
  CONSTRAINT `fk_area_country1`
    FOREIGN KEY (`country_id`)
    REFERENCES `telus-voting`.`country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `telus-voting`.`department`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `telus-voting`.`department` ;

CREATE TABLE IF NOT EXISTS `telus-voting`.`department` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Unique departement identifier.',
  `name` VARCHAR(25) NULL COMMENT 'Name of the department.',
  `area_id` INT NOT NULL COMMENT 'Area of the department.',
  PRIMARY KEY (`id`),
  INDEX `fk_department_area1_idx` (`area_id` ASC),
  CONSTRAINT `fk_department_area1`
    FOREIGN KEY (`area_id`)
    REFERENCES `telus-voting`.`area` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `telus-voting`.`person`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `telus-voting`.`person` ;

CREATE TABLE IF NOT EXISTS `telus-voting`.`person` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier for the person (internal use)',
  `uid` VARCHAR(30) NULL COMMENT 'Unique Identification Document number(for example DUI in El Salvador\'s case), this will be their username.\n',
  `password` VARCHAR(20) NULL COMMENT 'Password of the person.',
  `name` VARCHAR(45) NULL COMMENT 'Names of the person.',
  `surname` VARCHAR(45) NULL COMMENT 'Surnames of the person.',
  `department_id` INT NOT NULL COMMENT 'Department where the person lives.',
  PRIMARY KEY (`id`),
  INDEX `fk_person_department1_idx` (`department_id` ASC),
  UNIQUE INDEX `uid_UNIQUE` (`uid` ASC),
  CONSTRAINT `fk_person_department1`
    FOREIGN KEY (`department_id`)
    REFERENCES `telus-voting`.`department` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `telus-voting`.`committee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `telus-voting`.`committee` ;

CREATE TABLE IF NOT EXISTS `telus-voting`.`committee` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier of the committee',
  `name` VARCHAR(45) NULL COMMENT 'Name of the committee',
  `department_id` INT NOT NULL COMMENT 'Department of the committee (If a committee belongs to an area of a country, just select the first department that is related to the area you want)',
  PRIMARY KEY (`id`),
  INDEX `fk_committee_department1_idx` (`department_id` ASC),
  CONSTRAINT `fk_committee_department1`
    FOREIGN KEY (`department_id`)
    REFERENCES `telus-voting`.`department` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `telus-voting`.`candidate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `telus-voting`.`candidate` ;

CREATE TABLE IF NOT EXISTS `telus-voting`.`candidate` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Unique candidate identifier.',
  `email` VARCHAR(45) NULL COMMENT 'Contact email of the candidate.',
  `committee_id` INT NOT NULL COMMENT 'Committee of the candidate.',
  `person_id` INT NOT NULL COMMENT 'Personal information of the candidate.',
  PRIMARY KEY (`id`),
  INDEX `fk_candidate_committee1_idx` (`committee_id` ASC),
  INDEX `fk_candidate_person1_idx` (`person_id` ASC),
  CONSTRAINT `fk_candidate_committee1`
    FOREIGN KEY (`committee_id`)
    REFERENCES `telus-voting`.`committee` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_candidate_person1`
    FOREIGN KEY (`person_id`)
    REFERENCES `telus-voting`.`person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `telus-voting`.`election_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `telus-voting`.`election_category` ;

CREATE TABLE IF NOT EXISTS `telus-voting`.`election_category` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier for the election category.',
  `name` VARCHAR(20) NULL COMMENT 'Title of the election',
  `description` VARCHAR(50) NULL COMMENT 'A  short description of the election process ',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `telus-voting`.`election`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `telus-voting`.`election` ;

CREATE TABLE IF NOT EXISTS `telus-voting`.`election` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier for the election.',
  `name` VARCHAR(25) NULL COMMENT 'Title of the election.',
  `description` VARCHAR(50) NULL COMMENT 'A short description of the election.',
  `start_date` VARCHAR(45) NULL COMMENT 'Date when the election begins.',
  `end_date` VARCHAR(45) NULL COMMENT 'Date when the election ends.',
  `election_category_id` INT NOT NULL COMMENT 'Category of the election.',
  PRIMARY KEY (`id`),
  INDEX `fk_election_election_category1_idx` (`election_category_id` ASC),
  CONSTRAINT `fk_election_election_category1`
    FOREIGN KEY (`election_category_id`)
    REFERENCES `telus-voting`.`election_category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `telus-voting`.`vote`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `telus-voting`.`vote` ;

CREATE TABLE IF NOT EXISTS `telus-voting`.`vote` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Unique vote identifier.',
  `date` DATE NULL COMMENT 'Date when the vote is made.',
  `candidate_id` INT NOT NULL COMMENT 'Candidate selected.',
  `person_id` INT NOT NULL COMMENT 'Person who is voting.',
  `election_id` INT NOT NULL COMMENT 'Id of the election process.',
  PRIMARY KEY (`id`),
  INDEX `fk_votes_candidate1_idx` (`candidate_id` ASC),
  INDEX `fk_votes_person1_idx` (`person_id` ASC),
  INDEX `fk_vote_election1_idx` (`election_id` ASC),
  UNIQUE INDEX `unique_vote_index` (`candidate_id` ASC, `person_id` ASC, `election_id` ASC),
  CONSTRAINT `fk_votes_candidate1`
    FOREIGN KEY (`candidate_id`)
    REFERENCES `telus-voting`.`candidate` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_votes_person1`
    FOREIGN KEY (`person_id`)
    REFERENCES `telus-voting`.`person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_vote_election1`
    FOREIGN KEY (`election_id`)
    REFERENCES `telus-voting`.`election` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
