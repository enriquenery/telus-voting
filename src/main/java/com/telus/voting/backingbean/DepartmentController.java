package com.telus.voting.backingbean;

import com.telus.voting.backingbean.util.MobilePageController;
import entity.Department;
import entity.Vote;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "departmentController")
@ViewScoped
public class DepartmentController extends AbstractController<Department> {

    @Inject
    private AreaController areaIdController;
    @Inject
    private MobilePageController mobilePageController;

    public DepartmentController() {
        // Inform the Abstract parent controller of the concrete Department Entity
        super(Department.class);
    }

    @PostConstruct
    public void init() {
        this.setSelected(new Department());
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        areaIdController.setSelected(null);
    }

    /**
     * Sets the "items" attribute with a collection of Committee entities that
     * are retrieved from Department?cap_first and returns the navigation
     * outcome.
     *
     * @return navigation outcome for Committee page
     */
    public String navigateCommitteeList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Committee_items", this.getSelected().getCommitteeList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/committee/index";
    }

    /**
     * Sets the "items" attribute with a collection of Person entities that are
     * retrieved from Department?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Person page
     */
    public String navigatePersonList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Person_items", this.getSelected().getPersonList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/person/index";
    }

    /**
     * Sets the "selected" attribute of the Area controller in order to display
     * its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareAreaId(ActionEvent event) {
        if (this.getSelected() != null && areaIdController.getSelected() == null) {
            areaIdController.setSelected(this.getSelected().getAreaId());
        }
    }
}
