package com.telus.voting.backingbean;

import com.telus.voting.backingbean.util.MobilePageController;
import com.telus.voting.sessionbean.AreaFacade;
import com.telus.voting.sessionbean.DepartmentFacade;
import com.telus.voting.sessionbean.PersonFacade;
import entity.Area;
import entity.Country;
import entity.Department;
import entity.Person;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "personController")
@ViewScoped
public class PersonController extends AbstractController<Person> {

    //VARIABLES
    //Selected
    private Person selectedPerson = new Person();

    //Selected Item variable declaration
    private Country selectedCountry = new Country();
    private Area selectedArea = new Area();
    private Department selectedDepartment = new Department();

    //Selected Result Lists
    private List<Area> areaList = new <Area>ArrayList();
    private List<Department> departmentList = new <Department>ArrayList();

//CONTROLLER INJECTIONS
    @Inject
    private DepartmentController departmentIdController;
    @Inject
    private MobilePageController mobilePageController;

    //INJECTIONS
    @Inject
    private DepartmentFacade departmentFacade;
    @Inject
    private AreaFacade areaFacade;
    @Inject
    private PersonFacade pf;

    public PersonController() {
        // Inform the Abstract parent controller of the concrete Person Entity
        super(Person.class);
    }

    @PostConstruct
    public void init() {
        this.setSelected(new Person());
        selectedPerson = new Person();
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        departmentIdController.setSelected(null);
    }

    /**
     * Sets the "items" attribute with a collection of Candidate entities that
     * are retrieved from Person?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Candidate page
     */
    public String navigateCandidateList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Candidate_items", this.getSelected().getCandidateList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/candidate/index";
    }

    /**
     * Sets the "selected" attribute of the Department controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareDepartmentId(ActionEvent event) {
        if (this.getSelected() != null && departmentIdController.getSelected() == null) {
            departmentIdController.setSelected(this.getSelected().getDepartmentId());
        }
    }

    /**
     * Sets the "items" attribute with a collection of Vote entities that are
     * retrieved from Person?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Vote page
     */
    public String navigateVoteList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Vote_items", this.getSelected().getVoteList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/vote/index";
    }

    public void validateAndSave() {

        System.err.println("SELECTED PERSON: " + this.getSelected().getId());

        if (pf.findByIDPerson(this.getSelected())) { //If the insert element is repeated
            System.err.println("ID DEL USUARIO YA EXISTE!");
        } else {
            this.saveNew();
        }
    }

    //METHODS 
    //Ajax methods
    public void onCountryChange() {
        this.areaList = areaFacade.findByCountry(selectedCountry);
    }

    public void onAreaChange() {
        this.departmentList = departmentFacade.findByArea(selectedArea);
    }
    
    //Save Methods
    public void personRegister(){
    
    }

    //GETTERS AND SETTERS
    public Person getSelectedPerson() {
        return selectedPerson;
    }

    public void setSelectedPerson(Person selectedPerson) {
        this.selectedPerson = selectedPerson;
    }

    public Country getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(Country selectedCountry) {
        this.selectedCountry = selectedCountry;
    }

    public Area getSelectedArea() {
        return selectedArea;
    }

    public void setSelectedArea(Area selectedArea) {
        this.selectedArea = selectedArea;
    }

    public Department getSelectedDepartment() {
        return selectedDepartment;
    }

    public void setSelectedDepartment(Department selectedDepartment) {
        this.selectedDepartment = selectedDepartment;
    }

    public List<Area> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<Area> areaList) {
        this.areaList = areaList;
    }

    public List<Department> getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }

}
